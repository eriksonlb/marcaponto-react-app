
import React from 'react';
import './styles/navbar.css';

export default () => ( 

      <React.Fragment>
        <div className="Container">
          <div className="Menu">
            <img src={require('./images/one_line_logo.png')} className="MenuLogo" alt="home"/>
            <div className="MenuRight">
              <ul className="MenuList">
                <li className="MenuListItem">
                  <a href="/cadastros" className="MenuLink">Cadastros</a>
                </li>
                <li className="MenuListItem">
                  <a href="/consultas" className="MenuLink">Consultas</a>
                </li>
                <li className="MenuListItem">
                  <a href="/logout" className="MenuLinkLogout">Logout</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  
