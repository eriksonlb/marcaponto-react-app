import React from 'react'

import './styles/home.css'
import Page from './layout/Page'
import Area from './layout/Area'

export default props => {
    const user = localStorage.getItem('app-user');
    const username = user[0].toUpperCase() + user.slice(1); 
    return (
    <Page>
        <Area>
            <div className="Welcome">
                <h1>Olá <text className="userName">{username}</text>, seja bem vindo!</h1>
            </div>
        </Area>
    </Page>
    )
}