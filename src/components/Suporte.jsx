 
import React from 'react'

import './styles/home.css'
import Page from './layout/Page'
import Area from './layout/Area'
import PageTitle from './PageTitle'

export default props =>
    <Page>
        <div className="Suporte">
            <PageTitle pageTitle="Suporte" />   
            <Area>
            </Area>
        </div>
    </Page>