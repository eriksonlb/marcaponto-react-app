import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './styles/react-tabs.css';
import Pessoa from './abas/Pessoa'
import Colaborador from './abas/Colaborador'
import Expediente from './abas/Expediente'
import Funcao from './abas/Funcao'
import Horario from './abas/Horario'
import Setor from './abas/Setor'

export default props =>
  <Tabs classID="tabs">
    <TabList classID="tab-list">
      <Tab classID="tab-item">Pessoa</Tab>
      <Tab classID="tab-item">Setor</Tab>
      <Tab classID="tab-item">Função</Tab>
      <Tab classID="tab-item">Expediente</Tab>
      <Tab classID="tab-item">Colaborador</Tab>
      <Tab classID="tab-item">Horário</Tab>
    </TabList>

    <TabPanel classID="tab-panel">
      <Pessoa pageName={props.pageTitle}></Pessoa>
    </TabPanel>
    <TabPanel classID="tab-panel">
      <Setor pageName={props.pageTitle}></Setor>
    </TabPanel>
    <TabPanel classID="tab-panel">
      <Funcao pageName={props.pageTitle}></Funcao>
    </TabPanel>
    <TabPanel classID="tab-panel">
      <Expediente pageName={props.pageTitle}></Expediente>
    </TabPanel>
    <TabPanel classID="tab-panel">
      <Colaborador pageName={props.pageTitle}></Colaborador>
    </TabPanel>
    <TabPanel classID="tab-panel">
      <Horario pageName={props.pageTitle}></Horario>
    </TabPanel>
  </Tabs>