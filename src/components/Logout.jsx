export default () => {
    localStorage.removeItem('app-token')
    location.replace('/login')

}