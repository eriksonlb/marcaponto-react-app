import axios from 'axios';

const token = localStorage.getItem('app-token');
const api = axios.create({
    baseURL: 'https://ws-marcaponto.herokuapp.com/api/v1/',
    headers: {
        'authorization': token
      }
});

export default api;
