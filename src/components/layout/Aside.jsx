import React from 'react'
import '../styles/grid.css'

export default props =>
        <aside className="Aside">{props.children}</aside>
