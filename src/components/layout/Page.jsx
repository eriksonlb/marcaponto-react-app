import React from 'react'
import '../styles/page.css'

export default props =>
    <div>
        <div className="Page">
            {props.children}
        </div>
    </div>