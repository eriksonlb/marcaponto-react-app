import React from 'react'

export default props =>
        <header className="Header">{props.children}</header>
