import React from 'react'
import '../styles/area.css'


export default props =>
    <div>
        <div className="Area">
            {props.children}
        </div>
    </div>