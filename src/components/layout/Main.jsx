import React from 'react'
import '../styles/grid.css'

export default props =>
<main className="Main">{props.children}</main>
