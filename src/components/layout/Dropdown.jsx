import React from 'react'
import 'react-dropdown/style.css';
import Dropdown from 'react-dropdown';

export default props =>
<Dropdown options={props.options} onChange={this._onSelect} value={props.default} placeholder={props.text} />;



