import React from 'react'

import './styles/home.css'
import Page from './layout/Page'
import Area from './layout/Area'
import Abas from './Abas'
import PageTitle from './PageTitle'

export default props =>
    <Page>
        <div className="Cadastro">
            <PageTitle pageTitle="Cadastro" />   
            <Area>
                <Abas pageTitle="Cadastros">
                </Abas>
            </Area>
        </div>
    </Page>