import React from 'react'
import './styles/login.css'
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as yup from 'yup'
import axios from 'axios'

export default () => {
    const handleSubmit = values => {
        console.log(values)
        axios.post('https://ws-marcaponto.herokuapp.com/login', values)
            .then(resp => {
                const data = resp.headers['authorization']
                if (data) {
                    localStorage.setItem('app-token', data)
                    localStorage.setItem('app-user', values.username)
                    location.replace('/')
                }
                console.log(data)
            })
    }
    const validations = yup.object().shape({
        username: yup.string().required(),
        password: yup.string().min(5).required()
    })
    return (
        <div className="Login">
            <img src={require('./images/normal_logo.png')} className="LoginLogo" alt="home"/>
            <Formik
                initialValues={{}}
                onSubmit={handleSubmit}
                validationSchema={validations}
            >
                <Form className="LoginForm">
                    <div className="Login-Group">
                        <span className="Span">Username:</span>
                        <Field
                            name="username"
                            className="Login-Field"
                            />
                        <ErrorMessage
                            component="span"
                            name="username"
                            className="Login-Error"
                            />
                    </div>
                    <div className="Login-Group">
                        <span className="Span">Password:</span>
                        <Field
                            name="password"
                            className="Login-Field"
                            type="password"
                        />
                        <ErrorMessage
                            component="span"
                            name="password"
                            className="Login-Error"
                        />
                    </div>
                    <button className="Login-Btn" type="submit">Login</button>
                </Form>
            </Formik>
        </div>
    )
}
