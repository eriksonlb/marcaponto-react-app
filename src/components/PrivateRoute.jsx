import React from 'react'

import { Route, Redirect } from 'react-router'

export default props => {
    const isLogged = !!localStorage.getItem('app-token')
    return isLogged ? <Route {...props}/> : <Redirect to="/login"/>
}
