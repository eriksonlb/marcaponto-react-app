import React from 'react'
import './styles/pageTitle.css'

export default props =>
    <div className="Title">
        <p>{props.pageTitle}</p>
    </div>