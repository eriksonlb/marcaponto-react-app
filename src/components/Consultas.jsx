import React from 'react'

import './styles/home.css'
import Page from './layout/Page'
import Area from './layout/Area'
import Abas from './Abas'
import PageTitle from './PageTitle'

export default props =>
    <Page>
        <div className="Consultas">
            <PageTitle pageTitle="Consultas" />   
            <Area>
                <Abas pageTitle="Consultas" />
            </Area>
        </div>
    </Page>
