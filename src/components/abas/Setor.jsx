import React from 'react'
import Button from '@material-ui/core/Button';
import { Container, Row, Col } from 'react-grid-system';
import '../styles/area-grid.css'
import '../styles/setor.css'
import api from './../Api'

class Setor extends React.Component {
    constructor(){
        super();
        this.state = {
            setores: [],
            setor: {
            "nome": "",
            }
        };
        this.onChange = (evento) => {
            const state = Object.assign({}, this.state);
            const campo = event.target.name;
            state.setor[campo] = evento.target.value;
            this.setState(state.setor)
        };
        this.onSubmit = (evento) => {
            evento.preventDefault();
            api.post('setor', this.state.setor)
        }
    }

    async componentDidMount() {
        const response = await api.get('setor');
        this.setState({ setores: response.data });
    }

    render() {
        const { setores } = this.state;
        const renderSetor = (setor, index) => {
            return (
                <tr key={index}>
                    <td>{setor.nome}</td>
                    <td>{setor.ativo === true ? <p className="Ativo">Ativo</p> : <p>Inativo</p>}</td>
                </tr>
            )
        }
        return (
            <React.Fragment>
                {this.props.pageName === "Cadastros" ?
                <div>
                    <Container className="Ext">
                        <Row className="Row">
                            <Col sm={6} className="Col">
                                <div className="Nome">
                                    <text>Nome do Setor: </text>
                                    <input className="" name="nome" value={this.state.nome} onChange={this.onChange} type="text" />
                                </div>
                            </Col>
                            <Col sm={6} className="Col">
                                <div className="Buttons">
                                <Button variant="contained" color="secondary" className="CadastrarSetor" onClick={this.onSubmit}>Cadastrar</Button>
                                </div>

                            </Col>
                        </Row>
                    </Container>
                </div>
                :
                <div>
                        <Container className="Setor">
                        <h2>Setores Cadastrados</h2>
                        <table>
                        <thead>
                            <tr>
                                <th>Nome do Setor:</th>
                                <th>Status:</th>
                            </tr>
                        </thead>
                        <tbody>
                            {setores.map(renderSetor)}
                        </tbody>
                        </table>
                        </Container>
                    </div>
        }

            </React.Fragment>
        )
    }
}

export default Setor;