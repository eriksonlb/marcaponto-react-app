import React from 'react'
import Button from '@material-ui/core/Button';
import { Container, Row, Col } from 'react-grid-system';
import '../styles/area-grid.css'
import '../styles/expediente.css'
import api from './../Api'

class Expediente extends React.Component {
    constructor(){
        super();
        this.state = {
            expedientes: [],
            expediente: {
            "nome": "",
            "descricao": ""
            }
        };
        this.onChange = (evento) => {
            const state = Object.assign({}, this.state);
            const campo = event.target.name;
            state.expediente[campo] = evento.target.value;
            this.setState(state.expediente)
        };
        this.onSubmit = (evento) => {
            evento.preventDefault();
            const response = api.post('expediente', this.state.expediente)
            console.log(this.state.expediente)
            console.log(response)
        }
    }

    async componentDidMount() {
        const response2 = await api.get('/expediente');
        this.setState({ expedientes: response2.data });
    }

    render() {
        const { expedientes } = this.state;
        const renderExpediente = (expediente, index) => {
            return (
                <tr key={index}>
                    <td>{expediente.nome}</td>
                    <td>{expediente.descricao}</td>
                    <td>{expediente.ativo === true ? <p className="Ativo">Ativo</p> : <p>Inativo</p>}</td>
                </tr>
            )
        }
        return (
            <React.Fragment>
                {this.props.pageName === "Cadastros" ? 
                <div>
                    <Container className="Ext">
                        <Row className="Row">
                            <Col sm={7} className="Col">
                                <div className="Nome">
                                    <text>Nome do Expediente: </text>
                                    <input className="" 
                                            name="nome" 
                                            value={this.state.expediente.nome} 
                                            onChange={this.onChange} 
                                            type="text" placeholder="Ex: Administrativo" />
                                </div>
                            </Col>
                            <Col sm={5} className="Col">
                                <text>Descrição: </text>
                                <textarea name="descricao" placeholder="Ex: das 8h às 18h, 2h de almoço" value={this.state.expediente.descricao} onChange={this.onChange} >
                                    
                                </textarea>
                            </Col>
                        </Row>
                        <Row className="Row">
                            <Col sm={7} className="Col">
                            </Col>
                            <Col sm={5} className="Col">
                                <div className="Buttons">
                                    <Button onClick={this.onSubmit} variant="contained" color="secondary" className="CadastrarExpediente">Cadastrar</Button>
                                    <Button variant="contained" className="LimparExpediente">Limpar</Button>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                :
                <div>
                        <Container className="Expediente">
                        <h2>Expedientes Cadastrados</h2>
                        <table>
                        <thead>
                            <tr>
                                <th>Nome do Expediente</th>
                                <th>Descrição</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {expedientes.map(renderExpediente)}
                        </tbody>
                        </table>
                        </Container>
                    </div>
        }

            </React.Fragment>
        )
    }
}

export default Expediente;