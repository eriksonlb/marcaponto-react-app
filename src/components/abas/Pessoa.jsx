import React from 'react'
import Button from '@material-ui/core/Button';
import { Container, Row, Col } from 'react-grid-system';
import '../styles/pessoa.css'
import '../styles/area-grid.css'
import api from './../Api'


class Pessoa extends React.Component {

    constructor(){
        super();
        this.state = {
            pessoas: [],
            pessoa: {
            "nomeCompleto": "",
            "apelido": "",
            "email": "",
            "rg": "",
            "cpf": "",
            "dataNascimento": ""
            }
        };
        this.onChange = (evento) => {
            const state = Object.assign({}, this.state);
            const campo = event.target.name;
            state.pessoa[campo] = evento.target.value;
            this.setState(state.pessoa)
        };
        this.onSubmit = (evento) => {
            evento.preventDefault();
            api.post('pessoa', this.state.pessoa)
        }
    }

    async componentDidMount() {
        const response2 = await api.get('pessoa');
        this.setState({ pessoas: response2.data });
    }


    render() {
        const { pessoas } = this.state;
        const renderPessoa = (pessoa, index) => {
            return (
                <tr key={index}>
                    <td>{pessoa.nomeCompleto}</td>
                    <td>{pessoa.cpf}</td>
                    <td>{pessoa.dataNascimento}</td>
                    <td>{pessoa.ativo === true ? <p className="Ativo">Ativo</p> : <p>Inativo</p>}</td>
                </tr>
            )
        }

        return (
            <React.Fragment>
                {this.props.pageName === "Cadastros" ?
                    <div>
                        <Container className="Ext">
                            <Row className="Row">
                                <Col sm={6} className="Col">
                                    <text>Nome Completo: </text><input className="" name="nomeCompleto" value={this.state.nomeCompleto} onChange={this.onChange} type="text" />
                                </Col>
                                <Col sm={6} className="Col">
                                    <text>E-mail: </text><input name="email" value={this.state.email} onChange={this.onChange} type="text" />
                                </Col>
                            </Row>
                            <Row className="Row">
                                <Col sm={4} className="Col">
                                    <text>Apelido: </text><input className="" name="apelido" value={this.state.apelido} onChange={this.onChange} type="text" />

                                </Col>
                                <Col sm={4} className="Col">
                                    <text>Rg: </text><input name="rg" value={this.state.rg} onChange={this.onChange} type="text" />

                                </Col>
                                <Col sm={4} className="Col">
                                    <text>Cpf: </text><input name="cpf" value={this.state.cpf} onChange={this.onChange} type="text" />

                                </Col>
                            </Row>
                            <Row className="Row">
                                <Col sm={6} className="Col">
                                    <text className="DataLabel">Data de Nascimento: </text><input name="dataNascimento" onChange={this.onChange} value={this.state.dataNascimento} />
                                </Col>
                                <Col sm={6} className="Col">
                                    <Button variant="contained" className="LimparPessoa">Limpar</Button>
                                    <Button variant="contained" color="secondary" className="CadastrarPessoa" onClick={this.onSubmit}>Cadastrar</Button>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    :
                    <div>
                        <Container className="Pessoa">
                        <h2>Pessoas Cadastradas</h2>
                        <table>
                        <thead>
                            <tr>
                                <th>Nome:</th>
                                <th>Cpf:</th>
                                <th>Data Nascimento:</th>
                                <th>Status:</th>
                            </tr>
                        </thead>
                        <tbody>
                            {pessoas.map(renderPessoa)}
                        </tbody>
                        </table>
                        </Container>
                    </div>
                }
            </React.Fragment>
        )
    }
}

export default Pessoa;