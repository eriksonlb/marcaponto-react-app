import React from 'react'
import Dropdown from 'react-dropdown';
import Button from '@material-ui/core/Button';
import { Container, Row, Col } from 'react-grid-system';
import '../styles/horario.css'
import '../styles/area-grid.css'
import * as ReactBootStrap from 'react-bootstrap'
import api from './../Api'

const opcoesRegistro = [
    'Entrada', 'Intervalo', 'Retorno', 'Saída'
];
const opcoesDias = [
    'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'
];
const defaultRegistro = opcoesRegistro[0];
const defaultDias = opcoesDias[0];

class Horario extends React.Component {
    constructor() {
        super();
        this.state = {
            horarios: [],
            expedientes:[]
        };
    }
    
    async componentDidMount() {
        const response = await api.get('/horario');
        this.setState({ horarios: response.data });
        const response2 = await api.get('/expediente');
        this.setState({ expedientes: response2.data });
    }
    
    render() {
        const { horarios } = this.state;
        const renderHorario = (horario, index) => {
            return (
                <tr key={index}>
                    <td>{horario.diaDaSemana}</td>
                    <td>{horario.horario}</td>
                    <td>{horario.horarioMax}</td>
                    <td>{horario.horarioMin}</td>
                    <td>{horario.tipoRegistro}</td>
                    <td>{horario.toleranciaAtraso}</td>
                    <td>{horario.toleranciaExtra}</td>
                </tr>
            )
        }
        
        const expedientes = this.state.expedientes
        const opcoesExpediente = []  
        expedientes.forEach(function(expendiente){
            if(expendiente['ativo'] === true)
            opcoesExpediente.push(expendiente['nome'])
        })
        const defaultExpediente = opcoesExpediente[0];
        return (
            <React.Fragment>
                {this.props.pageName === "Cadastros" ? 
                <div>
                    <Container className="Ext">
                        <Row className="Row">
                            <Col sm={4} className="Col">
                                <text>Expediente: </text>
                                <Dropdown options={opcoesExpediente}
                                    className="Dropdown"
                                    onChange={this._onSelect}
                                    value={defaultExpediente}
                                    placeholder="Select an option" />
                            </Col>
                            <Col sm={4} className="Col">
                                <text>Dia da Semana: </text>
                                <Dropdown options={opcoesDias}
                                    onChange={this._onSelect}
                                    className="Dropdown"
                                    value={defaultDias}
                                    placeholder="Select an option" />
                            </Col>
                            <Col sm={4} className="Col">
                                <text>Tipo Registro: </text>
                                <Dropdown options={opcoesRegistro}
                                    onChange={this._onSelect}
                                    className="Dropdown"
                                    value={defaultRegistro}
                                    placeholder="Select an option" />
                            </Col>
                        </Row>
                        <Row className="Row">
                            <Col sm={4} className="Col">
                                <text>Horário minímo: </text>
                                <input type="time" id="appt" name="appt"
                                    min="07:00" max="18:00" required />
                            </Col>
                            <Col sm={4} className="Col">
                                <text>Horário máximo: </text>
                                <input type="time" id="appt" name="appt"
                                    min="08:00" max="18:00" required />
                            </Col>
                            <Col sm={4} className="Col">
                                <text>Horário: </text>
                                <input type="time" id="appt" name="appt"
                                    min="09:00" max="18:00" required />
                            </Col>
                        </Row>
                        <Row className="Row">
                            <Col sm={4} className="Col">
                                <text>Tolerância Extra: </text>
                                <input type="time" id="appt" name="appt"
                                    min="10:00" max="18:00" required />
                            </Col>
                            <Col sm={4} className="Col">
                                <text>Tolerância Atraso: </text>
                                <input type="time" id="appt" name="appt"
                                    min="11:00" max="18:00" required />
                            </Col>
                            <Col sm={4} className="Col">
                                <Button variant="contained" color="secondary" className="CadastrarColaborador">Cadastrar</Button>

                            </Col>
                        </Row>
                    </Container>
                </div>
                :
                <div>
                        <Container className="Horario">
                        <h2>Pessoas Cadastradas</h2>
                        <ReactBootStrap.Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Dia:</th>
                                <th>Horário:</th>
                                <th>Max:</th>
                                <th>Min:</th>
                                <th>Tipo:</th>
                                <th>Atraso:</th>
                                <th>Extra:</th>
                            </tr>
                        </thead>
                        <tbody>
                            {horarios.map(renderHorario)}
                        </tbody>
                        </ReactBootStrap.Table>
                        </Container>
                    </div>
        }
            </React.Fragment>
        )
    }
}

export default Horario;