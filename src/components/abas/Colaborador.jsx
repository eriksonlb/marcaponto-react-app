import React from 'react'
import Dropdown from 'react-dropdown';
import Button from '@material-ui/core/Button';
import { Container, Row, Col } from 'react-grid-system';
import '../styles/colaborador.css'
import '../styles/area-grid.css'
import api from './../Api'

class Colaborador extends React.Component {
    constructor() {
        super();
        this.state = {
            pessoas: [],
            funcoes: [],
            expedientes: [],
            colaborador: {
                "pessoaId": "",
                "funcaoId": "",
                "expedienteId": ""
            }
        };
        this.onChange = (evento) => {
            const state = Object.assign({}, this.state);
            const campo = event.target.name;
            state.colaborador[campo] = evento.target.value;
            this.setState(state.funcao)
        };
        this.onSubmit = (evento) => {
            evento.preventDefault();
            api.post('colaborador', this.state.colaborador)
        }
    }

    async componentDidMount() {
        const response = await api.get('/pessoa');
        this.setState({ pessoas: response.data });
        const response2 = await api.get('/funcao');
        this.setState({ funcoes: response2.data });
        const response3 = await api.get('/expediente');
        this.setState({ expedientes: response3.data });
    }

    render() {
        const pessoas = this.state.pessoas
        const nomePessoas = []
        pessoas.forEach(function (pessoa) {
            if (pessoa['ativo'] === true)
                nomePessoas.push(pessoa['apelido'])
        })

        const funcoes = this.state.funcoes
        const nomeFuncoes = []
        funcoes.forEach(function (funcao) {
            if (funcao['ativo'] === true)
                nomeFuncoes.push(funcao['nome'])
        })

        const expedientes = this.state.expedientes
        const nomeExpedientes = []
        expedientes.forEach(function (expendiente) {
            if (expendiente['ativo'] === true)
                nomeExpedientes.push(expendiente['nome'])
        })
        const defaultPessoa = nomePessoas[0];
        const defaultFuncao = nomeFuncoes[0];
        const defaultExpediente = nomeExpedientes[0];

        const { colaboradores } = this.state;
        const renderColaborador = (colaborador, index) => {
            return (
                <tr key={index}>
                    <td>{colaborador.nome}</td>
                    <td>{colaborador.ativo === true ? <p className="Ativo">Ativo</p> : <p>Inativo</p>}</td>
                </tr>
            )
        }
        return (
            <React.Fragment>
                {this.props.pageName === "Cadastros" ?
                    <div>
                        <Container className="Ext">
                            <Row className="Row">
                                <Col sm={4} className="Col">
                                    <text>Pessoa: </text>
                                    <Dropdown options={nomePessoas}
                                        className="Dropdown"
                                        onChange={this._onSelect}
                                        value={defaultPessoa}
                                        placeholder="Select an option" />
                                </Col>
                                <Col sm={4} className="Col">
                                    <text>Função </text>
                                    <Dropdown options={nomeFuncoes}
                                        onChange={this._onSelect}
                                        className="Dropdown"
                                        value={defaultFuncao}
                                        placeholder="Select an option" />
                                </Col>
                                <Col sm={4} className="Col">
                                    <text>Expediente </text>
                                    <Dropdown options={nomeExpedientes}
                                        className="Dropdown"
                                        onChange={this._onSelect}
                                        value={defaultExpediente}
                                        placeholder="Select an option" />
                                </Col>
                            </Row>
                            <Row className="Row">
                                <Col sm={4} className="Col">
                                </Col>
                                <Col sm={4} className="Col">
                                </Col>
                                <Col sm={4} className="Col">
                                    <Button variant="contained" color="secondary" className="CadastrarColaborador">Cadastrar</Button>

                                </Col>
                            </Row>
                        </Container>
                    </div>
                    :
                    <div>
                        <Container className="Colaborador">
                            <h2>Colaboradores Cadastrados</h2>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Nome:</th>
                                        <th>Status:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Erikson Lopes</td>
                                        <td>Ativo</td>
                                    </tr>
                                    <tr>
                                        <td>Heitor Amaral</td>
                                        <td>Ativo</td>
                                    </tr>
                                    <tr>
                                        <td>Juarez Junior</td>
                                        <td>Ativo</td>
                                    </tr>
                                    <tr>
                                        <td>Lucas Amstalden</td>
                                        <td>Ativo</td>
                                    </tr>
                                    <tr>
                                        <td>Nicolas Marqui</td>
                                        <td>Ativo</td>
                                    </tr>
                                </tbody>
                            </table>
                        </Container>
                    </div>
                }

            </React.Fragment>
        )
    }
}

export default Colaborador;