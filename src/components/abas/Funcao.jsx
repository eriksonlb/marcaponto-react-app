import React from 'react'
import Button from '@material-ui/core/Button';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { Container, Row, Col } from 'react-grid-system';
import '../styles/area-grid.css'
import '../styles/funcao.css'
import api from './../Api'

const Checkbox = props => (
    <input type="checkbox" {...props} />
)

class Funcao extends React.Component {
    constructor() {
        super();
        this.state = {
            funcoes: [],
            setores:[],
            funcao: {
                "nome": "",
            }
        };
        this.onChange = (evento) => {
            const state = Object.assign({}, this.state);
            const campo = event.target.name;
            state.funcao[campo] = evento.target.value;
            this.setState(state.funcao)
        };
        this.onSubmit = (evento) => {
            evento.preventDefault();
            api.post('setor', this.state.funcao)
        }
    }

    async componentDidMount() {
        const response = await api.get('/funcao');
        this.setState({ funcoes: response.data });
        const response2 = await api.get('/setor');
        this.setState({ setores: response2.data });
    }



    render() {
        const nomeFuncoes = []
        this.state.funcoes.forEach(function(funcao){
            if(funcao['ativo'] === true)
                nomeFuncoes.push(funcao['nome'])
        })
        this.optionsFuncoes = nomeFuncoes;
        this.defaultOption = this.optionsFuncoes[0];


        const { setores } = this.state;
        const nomeSetores = []
        setores.forEach(function(setor){
            if(setor['ativo'] === true)
                nomeSetores.push(setor['nome'])
        })
        this.optionsSetores = nomeSetores;
        this.defaultOptionSetor = this.optionsSetores[0];

        const renderFuncao = (funcao, index) => {
            return (
                <tr key={index}>
                    <td>{funcao.nome}</td>
                    <td>{funcao.responsavel === true ? <p className="Ativo">Sim</p> : <p>Não</p>}</td>
                    <td>{funcao.ativo === true ? <p className="Ativo">Ativo</p> : <p>Inativo</p>}</td>
                </tr>
            )
        }
        return (
            <React.Fragment>
                {
                    this.props.pageName === "Cadastros" ?
                        <div>
                            <Container className="Ext">
                                <Row className="Row">
                                    <Col md={6} className="Col">
                                        <div className="Funcao">
                                            <text>Nome da Função: </text><input className="" name="nomeFuncao" value={this.state.nomeCompleto} onChange={this.onChange} type="text" />
                                        </div>
                                    </Col>
                                    <Col md={3} className="Col">
                                        <div className="Função Responsável">
                                            <text>Função Responsável: </text>
                                            <Dropdown options={this.optionsFuncoes} onChange={this._onSelect} value={this.defaultOption} placeholder="Selecionar" />
                                        </div>

                                    </Col>
                                    <Col md={3} className="Col">
                                        <div className="Setor">
                                            <text>Setor </text>
                                            <Dropdown options={this.optionsSetores} onChange={this._onSelect} value={this.defaultOptionSetor} placeholder="Selecionar" />
                                        </div>

                                    </Col>
                                </Row>
                                <Row className="Row">
                                    <Col md={6} className="Col">
                                        <div className="Checkbox">
                                            <label>
                                                <Checkbox
                                                    checked={this.state.checked}
                                                    onChange={this.handleCheckboxChange}
                                                />
                                                <span>Responsável</span>
                                            </label>
                                        </div>

                                    </Col>
                                    <Col md={6} className="Col">
                                        <div className="Buttons">
                                            <Button variant="contained" color="secondary" className="CadastrarFuncao">Cadastrar</Button>
                                            <Button variant="contained" className="LimparFuncao">Limpar</Button>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        :
                        <div>
                            <Container className="Funcao">
                                <h2>Setores Cadastrados</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Nome da Função</th>
                                            <th>Responsável</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.funcoes.map(renderFuncao)}
                                    </tbody>
                                </table>
                            </Container>
                        </div>
                }

            </React.Fragment >
        )
    }
}

export default Funcao;