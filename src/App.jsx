import React from 'react'
import './App.css'
import './components/styles/grid.css'
import { Switch, Route } from 'react-router-dom'
import PrivateRoute from './components/PrivateRoute';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Cadastros from './components/Cadastro';
import Consultas from './components/Consultas';
import Suporte from './components/Suporte';
import Login from './components/Login';
import Logout from './components/Logout';
import Header from './components/layout/Header'
import Aside from './components/layout/Aside'
import Main from './components/layout/Main'

export default () => (
    <div className="App">
        {/* <Navbar />
        <Home userName="Erikson"></Home>
        <Cadastro /> */}
        <div className="Grid">
            <Header>
                <Switch>
                <PrivateRoute component={Navbar}></PrivateRoute>
                </Switch>
                </Header>
            <Aside></Aside>
            <Main>
                    <Switch>
                        <Route path="/login" component={Login}></Route>
                        <PrivateRoute exact path="/" component={Home}></PrivateRoute>
                        <PrivateRoute path="/cadastros" component={Cadastros}></PrivateRoute>
                        <PrivateRoute path="/consultas" component={Consultas}></PrivateRoute>
                        <PrivateRoute path="/suporte" component={Suporte}></PrivateRoute>
                        <PrivateRoute path="/logout" component={Logout}></PrivateRoute>
                    </Switch>

            </Main>
        </div>

    </div>
)